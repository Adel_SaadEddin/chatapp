import datetime

from django.contrib.auth.models import User
from django.contrib.humanize.templatetags import humanize
from django.db import models

class Chat(models.Model):
    admin = models.ForeignKey(User, on_delete=models.CASCADE, )
    participants = models.ManyToManyField(User, related_name='chats')
    name = models.CharField(max_length=128, default=None)
    created = models.DateTimeField(auto_now_add=True)

    @property
    def participants_count(self):
        return self.participants.count()

    @property
    def participants_summary(self):
        count = self.participants.count()
        first = self.participants.all().order_by('id')[:3]

        res = ""
        for user in first:
            res += user.first_name + ", "

        if count > len(first):
            res += " and {} others".format(count - len(first))

        return res


class Message(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='user')
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name='messages', null=True)
    text = models.CharField(max_length=4096)
    date = models.DateTimeField(auto_now_add=True)

    @property
    def human_readable_date(self):
        return humanize.naturalday(datetime.datetime.now() - self.date)
