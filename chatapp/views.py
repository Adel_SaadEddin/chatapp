from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect

from chatapp.models import Chat, Message
from chatapp.serializers import MessageSerializer


def hello_world(request):
    return render(request, 'hello_world.html', {})


def users(request):
    return render(request, 'users.html', {users: User.objects})


@login_required
def create_chat(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        if name:
            chat = Chat(name=name, admin_id=request.user.id)
            chat.save()
            for key in dict(request.POST.items()):
                if key.isdigit():
                    user = User.objects.get(id=key)
                    print(key, user)
                    chat.participants.add(user)
            chat.participants.add(request.user)
            chat.save()

        return HttpResponseRedirect("/")

    context = {
        "users": User.objects.all().exclude(id=request.user.id)
    }
    return render(request, 'create_chat.html', context)


@login_required
def index(request, id=None):
    user = request.user

    context = {
        "chats": user.chats.all(),
        "chat": Chat.objects.get(id=id) if id else None,
        "messages": Message.objects.filter(chat_id=id) if id else [],
        "current_user": user,
    }

    return render(request, 'index.html', context)


@login_required
def send(request, id):
    chat = get_object_or_404(Chat, pk=id)
    user = request.user
    text = request.POST.get('text')
    message = Message(user_id=user.id, chat_id=chat.id, text=text)
    message.save()

    return chat_data(request, id)


@login_required
def chat_data(request, id):
    chat = get_object_or_404(Chat, pk=id)
    messages = chat.messages.order_by('date').all()
    result = MessageSerializer(messages, many=True).data

    return JsonResponse({'messages': list(result)})


def seed(request):
    data = {
        "users": [
            {
                "username": "adel-saadeddin",
                "password": "123456",
                "first_name": "Adel",
                "last_name": "SaadEddin"
            },
            {
                "username": "zaher-saadeddin",
                "password": "123456",
                "first_name": "Zaher",
                "last_name": "SaadEddin"
            },
            {
                "username": "hell",
                "password": "123456",
                "first_name": "Hell",
                "last_name": "Jad"
            },
        ]
    }

    for user_data in data["users"]:
        user, created = User.objects.get_or_create(
            username=user_data['username'],
            first_name=user_data['first_name'],
            last_name=user_data['last_name'],
            is_staff=True,
            is_active=True,
            is_superuser=True,
        )
        user.set_password(user_data['password'])
        user.save()

        return render(request, 'index.html')


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}.")
                return redirect("/")
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request=request, template_name="login.html", context={"login_form": form})


def logout_request(request):
    logout(request)
    messages.info(request, "You have successfully logged out.")
    return redirect("/")
