from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer

from chatapp.models import Message


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')


class MessageSerializer(ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Message
        fields = ('id', 'text', 'user', 'date')
