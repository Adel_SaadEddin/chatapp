"""chatapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from chatapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),

    path('seed/', views.seed, name='seed'),

    path('', views.index, name='home'),
    path('chat', views.index, name='chat_list'),
    path('chat/<int:id>', views.index, name='chat'),
    path('chat/<int:id>/send', views.send, name='send'),
    path('chat/<int:id>/data', views.chat_data, name='chat_data'),
    path('chat/create', views.create_chat, name='create_chat'),

    path("login", views.login_request, name="login"),
    path("accounts/login", views.login_request, name="login1"),
    path("logout", views.logout_request, name="logout"),
]
